<?php

/**
 * @file
 * Contains flag_list_item.page.inc.
 *
 * Page callback for Flag list item entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Flag list item templates.
 *
 * Default template: flag_list_item.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_flag_list_item(array &$variables) {
  // Fetch FlagListItem Entity Object.
  $flag_list_item = $variables['elements']['#flag_list_item'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
