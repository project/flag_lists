<?php

/**
 * @file
 * Flag lists module tokens support.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function flag_lists_token_info() {

  // Flagging collection tokens.
  //
  // Nothing has to be done as every field from the entities
  // are automagically included.
  return [];
}

/**
 * Implements hook_token_info_alter().
 */
function flag_lists_token_info_alter(&$data) {
  //
  // Flagging collection tokens.
  //
  // Nothing has to be done as every field from the entities
  // are automagically included.
  $data['types']['flagging_collection'] = [
    'name' => t('Flagging Collection'),
    'description' => t('Tokens related to flagging collection data.'),
    'needs-data' => 'flagging_collection',
  ];
  $data['types']['flag_list_item'] = [
    'name' => t('Flag List Items'),
    'description' => t('Tokens related to flag list items.'),
    'needs-data' => 'flag_list_item',
  ];
}

/**
 * Implements hook_tokens().
 */
function flag_lists_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $tokenService = \Drupal::token();
  $url_options = [
    'absolute' => TRUE,
  ];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()
      ->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }
  $replacements = [];

  if ($type == 'flagging_collection' && !empty($data['flagging_collection'])) {

    /** @var \Drupal\flag_lists\Entity\FlaggingCollection $flc */
    $flc = $data['flagging_collection'];
    foreach ($tokens as $name => $original) {
      switch ($name) {

        // Simple key values on the node.
        case 'id':
          $replacements[$original] = $flc->id();
          break;

        case 'name':
          $replacements[$original] = $flc->getName();
          break;
      }
    }
  }
  return $replacements;
}

/**
 * Returns HTML for a tokens browser.
 *
 * @param array $variables
 *   An associative array containing:
 *   - types: An array naming the types of tokens to show.
 *   - global_types: Whether to show global tokens.
 */
function theme_flag_lists_tokens_browser(array $variables) {
  $types = $variables['types'];
  $global_types = $variables['global_types'];

  if (\Drupal::moduleHandler()->moduleExists('token')) {
    return theme('token_tree', [
      'token_types' => $types,
      'global_types' => $global_types,
    ]);
  }
  else {
    return '<p><em>' . t("Note: You don't have the <a href='@token-url'>Token</a> module installed, so the list of available tokens isn't shown here. You don't have to install <a href='@token-url'>Token</a> to be able to use tokens, but if you have it installed, and enabled, you'll be able to enjoy an interactive tokens browser.", ['@token-url' => 'http://drupal.org/project/token']) . '</em></p>';
  }
}
