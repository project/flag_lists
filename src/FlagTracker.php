<?php

namespace Drupal\flag_lists;

use Drupal\Core\Form\FormStateInterface;
use Drupal\flag_lists\Entity\FlagForList;

/**
 * Class FlagTracker.
 *
 * Tracks updating of flags.
 */
class FlagTracker {

  /**
   * {@inheritdoc}
   *
   * Add a FlagForLists record in the database.
   *
   * @param array $form
   *   The form associative array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The formState array.
   */
  public static function save(array $form, FormStateInterface $form_state) {

    $flagValues = $form_state->getValues();

    $build_array = [];
    $build_array['id'] = $flagValues['id'];
    $build_array['label'] = $flagValues['label'];

    $flagForList = new FlagForList($build_array, 'flag_for_list');

    $flagForList->setBaseFlag($flagValues['id']);
    $flagForList->setOwner();
    $flagForList->save();
  }

  /**
   * {@inheritdoc}
   *
   * Update a FlagForLists record in the database.
   *
   * @param array $form
   *   The form associative array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The formState array.
   * @param Drupal\flag_lists\FlagForList $flagTemplate
   *   The FlagForList to be update.
   */
  public static function update(array $form,
    FormStateInterface $form_state,
    FlagForList $flagTemplate) {
    $flagTemplate->set('label', $form_state->getValue('label'));
    $flagTemplate->setOwner();
    $flagTemplate->save();
  }

  /**
   * Delete a FlagForLists record in the database.
   */
  public function delete() {
  }

}
