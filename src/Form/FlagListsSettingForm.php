<?php

namespace Drupal\flag_lists\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;

/**
 * Class FlaggingCollectionSettingsForm.
 *
 * @ingroup flag_lists
 */
class FlagListsSettingForm extends ConfigFormBase {

  /**
   * The Cache Tags Invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidator
   *   The cache tags handler.
   */
  protected $cacheTagsInvalidator;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\EntityTypeManager
   *   The entity type manager.
   */
  protected $entityTypeManager;

  /**
   * The Entity Display Repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   *   The entity display repository.
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    CacheTagsInvalidator $cache_tags_invalidator,
    EntityTypeManager $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('cache_tags.invalidator'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['flag_lists.settings'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'flag_lists_settings_form';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('flag_lists.settings')
      ->set('hide_collections',
      $form_state->getValue('hide_collections'))
      ->save();
    $this->config('flag_lists.settings')
      ->set('hide_collections_in_displays',
      $form_state->getValue('hide_collections_in_displays'))
      ->save();
    $this->config('flag_lists.settings')
      ->set('hide_templates_in_displays',
      $form_state->getValue('hide_templates_in_displays'))
      ->save();

    $clearCacheLink = Link::createFromRoute(
      'cache',
      'system.performance_settings'
      )
      ->toString();
    $this->messenger()->addStatus($this
      ->t('If needed, please clear the @link!', ['@link' => $clearCacheLink]));

    // Only invalidating using the generic cache tag.
    $this->cacheTagsInvalidator
      ->invalidateTags(['config:entity_view_display_list']);

  }

  /**
   * Defines the settings form for Flagging collection entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('flag_lists.settings');

    $form['flag_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Overview Settings'),
    ];
    $flagOverviewLink = Link::createFromRoute(
      'Flag Overview',
      'entity.flag.collection'
      )
      ->toString();
    $form['flag_settings']['hide_collections'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Flagging Collections in the @link.',
         ['@link' => $flagOverviewLink]
      ),
      '#default_value' => $config->get('hide_collections'),
      '#description' => $this->t('Do you want to hide the Flagging Collections in the @link?',
        ['@link' => $flagOverviewLink]
      ),
    ];
    $flagCollectionsInDisplayLink = Link::createFromRoute(
      'Content Types Manage Display Forms',
      'entity.node_type.collection'
      )
      ->toString();
    $form['content_types'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Types Settings'),
    ];
    $form['content_types']['hide_collections_in_displays'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Flagging Collections in the @link as well as for other entities.',
         ['@link' => $flagCollectionsInDisplayLink]
      ),
      '#default_value' => $config->get('hide_collections_in_displays'),
      '#description' => $this->t('Do you want to hide the Flagging Collections in the @link as well as for other entities?',
        ['@link' => $flagCollectionsInDisplayLink]
      ),
    ];
    $form['content_types']['hide_templates_in_displays'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Flagging Templates in the @link as well as for other entities.',
         ['@link' => $flagCollectionsInDisplayLink]
      ),
      '#default_value' => $config->get('hide_templates_in_displays'),
      '#description' => $this->t('Do you want to hide the Flagging Templates in the @link as well as for other entities?',
        ['@link' => $flagCollectionsInDisplayLink]
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

}
