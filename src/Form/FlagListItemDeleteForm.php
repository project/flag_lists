<?php

namespace Drupal\flag_lists\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting Flag List Item entities.
 *
 * @ingroup flag_lists
 */
class FlagListItemDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {

    return $this
      ->t('Please note that this is only to delete erranious Flag List Items in the case upgrade problems. In the normal case you would like to "unflag" the Flag List Item instead of "delete" it. Are you still really sure you want to delete it?');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();

    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();
    $entity->delete();
    $message = $this->getDeletionMessage();
    $this->messenger()->addStatus($message);
    $this->logDeletionMessage();

  }

}
