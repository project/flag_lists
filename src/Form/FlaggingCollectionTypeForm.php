<?php

namespace Drupal\flag_lists\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for setting Flagging Collection Type properties.
 */
class FlaggingCollectionTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $flagging_collection_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $flagging_collection_type->label(),
      '#description' => $this->t("Label for the Flagging collection type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $flagging_collection_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\flag_lists\Entity\FlaggingCollectionType::load',
      ],
      '#disabled' => !$flagging_collection_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $flagging_collection_type = $this->entity;
    $status = $flagging_collection_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Flagging collection type.', [
            '%label' => $flagging_collection_type->label(),
          ]
          ));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Flagging collection type.', [
            '%label' => $flagging_collection_type->label(),
          ]
          ));
    }
    $form_state->setRedirectUrl($flagging_collection_type->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Verify that the name is not longer than 32 charaters.
    if (strlen($form_state->getValue('id')) > 32) {
      $form_state->setErrorByName('id', $this->t('Please provide an id that is maximum 32 characters long.'));
    }

    parent::validateForm($form, $form_state);
  }

}
