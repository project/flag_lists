<?php

namespace Drupal\flag_lists\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting Flagging collection entities.
 *
 * @ingroup flag_lists
 */
class FlaggingCollectionDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();

    // Build a list of Flag list items related to this connection.
    $flagListsService = \Drupal::service('flaglists');
    if ($entity->getRelatedFlag() == NULL) {
      // If this happen the database is seriously broken and must be fixed.
      return parent::buildForm($form, $form_state);
    }
    $flag_list_items = $flagListsService->getFlagListItemIds(
      $entity->getRelatedFlag()->id());

    if (!empty($flag_list_items)) {
      $output = [];
      $items = $flagListsService->getFlagListItems($flag_list_items);
      foreach ($items as $item) {
        $output[] = $item->getName();
      }

      $form['active_items'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $output,
        '#title' => $this->t('The following connected Flag List Items will be deleted as well:'),
        '#empty' => $this->t('No Flag List Items found'),
      ];
    }

    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();
    $message = $this->getDeletionMessage();

    // Make sure that deleting a translation does not delete the whole entity.
    if (!$entity->isDefaultTranslation()) {
      $untranslated_entity = $entity->getUntranslated();
      $untranslated_entity->removeTranslation($entity->language()->getId());
      $untranslated_entity->save();
      $form_state->setRedirectUrl($untranslated_entity->toUrl('canonical'));
    }
    else {
      $entity->delete();
      $form_state->setRedirectUrl($this->getRedirectUrl());
    }

    $flagListsService = \Drupal::service('flaglists');
    if ($entity->getRelatedFlag() !== NULL) {
      $flag_list_items = $flagListsService->getFlagListItemIds(
        $entity->getRelatedFlag()->id());
      if (!empty($flag_list_items)) {
        $items = $flagListsService->getFlagListItems($flag_list_items);
        foreach ($items as $item) {
          $item->delete();
        }
      }
    }
    else {
      // If this happen the database was seriously broken and must be checked.
      $this->logger('flag_lists')
           ->error('Your Related Flag for the Flag Lists "@flag_list" was missing!',
           ['@flag_list' => $entity->label()]);
    }
    $this->messenger()->addStatus($message);
    $this->logDeletionMessage();
  }

}
