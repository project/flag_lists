<?php

namespace Drupal\flag_lists\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete Flag for list entities.
 */
class FlagForListDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Check if this Flag For List is in use.
    $flagListService = \Drupal::service('flaglists');
    $flaggingCollections = $flagListService->getAllFlaggingCollections();

    $output = [];
    foreach ($flaggingCollections as $flag) {
      if ($flag->getBaseFlag()->id() == $this->entity->id()) {
        $output[] = $flag->getName();
      }
    }

    // If we are using this Flag For List let the user know.
    if (!empty($output)) {
      $form['active_items'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $output,
        '#title' => $this->t('Please remove the following Flagging Collections before proceding:'),
        '#empty' => $this->t('No Flag List Items found'),
      ];
    }

    $form = parent::buildForm($form, $form_state);

    if (!empty($output)) {
      // Unset the options for submitting the form.
      unset($form['actions']['submit']);
      unset($form['description']);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name and the corresponding Flag?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.flag_for_list.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();

    $this->messenger()->addMessage(
      $this->t('content @type: deleted @label.',
        [
          '@type' => $this->entity->bundle(),
          '@label' => $this->entity->label(),
        ]
        )
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
