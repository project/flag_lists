<?php

namespace Drupal\flag_lists\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Class FlagListsHelpController.
 *
 *  Returns Help for the Flag Lists.
 */
class FlagListsHelpController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Help for missing Related Flags.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function missingRelated() {
    $output = '';
    $output .= '<p>The <em>Related Flag</em> for your <em>Flag List</em> is ';
    $output .= 'missing. ';
    $output .= 'This is a severe error that most probably has been caused by ';
    $output .= 'removing the <em>Related Flag</em> directly instead of ';
    $output .= 'removing it by removing the <em>Flag List</em>.</p>';
    $output .= '<p>You can try to recreate the missing Related Flag by:';
    $output .= '<ol>';
    $output .= '<li>Go to the <a href="/flagging_collection">Flagging Collection entities page</a></li>';
    $output .= '<li>In the <em>Flagging Collection</em> column click on the Collection that state <em>Missing/Deleted</em> in the <em>The related flag</em> column.</li>';
    $output .= '<li>On the page that appears, click <em>Save</em> at the bottom.</li>';
    $output .= '<li>Go to the <a href="/flagging_collection">Flagging Collection entities page</a> again and check that the <em>The related flag</em> column now has a name!</li>';
    $output .= '</ol>';
    $output .= '</p>';
    $output .= '<p>If the above doesn\'t work, then the only remedy is ';
    $output .= 'to remove the <em>Flag List</em> and ';
    $output .= 'then recreate it. Note that by doing this also ';
    $output .= '<strong>all</strong> ';
    $output .= 'flaggings will be removed!</p>';

    return [
      '#markup' => $this->t($output)
    ];
  }

}
