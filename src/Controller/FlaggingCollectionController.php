<?php

namespace Drupal\flag_lists\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Element\Link;
use Drupal\Core\Url;
use Drupal\flag_lists\Entity\FlaggingCollectionInterface;

/**
 * Class FlaggingCollectionController.
 *
 *  Returns responses for Flagging collection routes.
 */
class FlaggingCollectionController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Flagging collection  revision.
   *
   * @param int $flagging_collection_revision
   *   The Flagging collection  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($flagging_collection_revision) {
    $flagging_collection = $this->entityTypeManager()->getStorage('flagging_collection')->loadRevision($flagging_collection_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('flagging_collection');

    return $view_builder->view($flagging_collection);
  }

  /**
   * Page title callback for a Flagging collection  revision.
   *
   * @param int $flagging_collection_revision
   *   The Flagging collection  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($flagging_collection_revision) {
    $flagging_collection = $this->entityTypeManager()->getStorage('flagging_collection')->loadRevision($flagging_collection_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $flagging_collection->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($flagging_collection->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Flagging collection .
   *
   * @param \Drupal\flag_lists\Entity\FlaggingCollectionInterface $flagging_collection
   *   A Flagging collection  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(FlaggingCollectionInterface $flagging_collection) {
    $account = $this->currentUser();
    $langcode = $flagging_collection->language()->getId();
    $langname = $flagging_collection->language()->getName();
    $languages = $flagging_collection->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $flagging_collection_storage = $this->entityTypeManager()->getStorage('flagging_collection');

    $build['#title'] = $has_translations ?
      $this->t('@langname revisions for %title', [
        '@langname' => $langname,
        '%title' => $flagging_collection->label(),
      ]) :
      $this->t('Revisions for %title', [
        '%title' => $flagging_collection->label(),
      ]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all flagging collection revisions") || $account->hasPermission('administer flagging collection entities')));
    $delete_permission = (($account->hasPermission("delete all flagging collection revisions") || $account->hasPermission('administer flagging collection entities')));

    $rows = [];

    $vids = $flagging_collection_storage->revisionIds($flagging_collection);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\flag_lists\FlaggingCollectionInterface $revision */
      $revision = $flagging_collection_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $flagging_collection->getRevisionId()) {
          $link = Link::fromTextAndUrl($date,
            new Url('entity.flagging_collection.revision', [
              'flagging_collection' => $flagging_collection->id(),
              'flagging_collection_revision' => $vid,
            ]
          ));
        }
        else {
          $link = $flagging_collection->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.flagging_collection.translation_revert', [
                'flagging_collection' => $flagging_collection->id(),
                'flagging_collection_revision' => $vid,
                'langcode' => $langcode,
              ]
              ) :
              Url::fromRoute('entity.flagging_collection.revision_revert', [
                'flagging_collection' => $flagging_collection->id(),
                'flagging_collection_revision' => $vid,
              ]
              ),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute(
                'entity.flagging_collection.revision_delete', [
                  'flagging_collection' => $flagging_collection->id(),
                  'flagging_collection_revision' => $vid,
                ]
              ),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['flagging_collection_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
