<?php

namespace Drupal\flag_lists\EventSubscriber;

use Drupal\flag\Event\FlaggingEvent;
use Drupal\flag\Event\UnflaggingEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\flag\Event\FlagEvents;

/**
 * Subscribe to Flag events.
 */
class FlagForListSubscriber implements EventSubscriberInterface {
  use MessengerTrait;

  /**
   * Constructs a new FlagForListSubscriber object.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[FlagEvents::ENTITY_FLAGGED][] = ['flagListsEntityFlagged', 100];
    $events[FlagEvents::ENTITY_UNFLAGGED][] = ['flagListsEntityUnflagged', 100];
    return $events;
  }

  /**
   * This is called whenever the flag.entity_flagged event is dispatched.
   *
   * @param \Drupal\flag\Event\FlaggingEvent $flag_event
   *   The response event.
   */
  public function flagListsEntityFlagged(FlaggingEvent $flag_event) {

    $flagId = $flag_event->getFlagging()->getFlagId();

    foreach (\Drupal::service('flaglists')->getAllFlagForList() as $collection) {
      if ($collection->getBaseFlag() == $flagId) {
        $this->messenger()->addMessage(t('Entity added to the %flaglist list.',
          ['%flaglist' => $flagId]));
      }
    }

  }

  /**
   * This is called whenever the flag.entity_unflagged event is dispatched.
   *
   * @param \Drupal\flag\Event\UnflaggingEvent $flag_events
   *   The response events.
   */
  public function flagListsEntityUnflagged(UnflaggingEvent $flag_events) {
    foreach ($flag_events->getFlaggings() as $flagging) {

      $flagId = $flagging->getFlagId();

      foreach (\Drupal::service('flaglists')->getAllFlagForList() as $collection) {
        if ($collection->getBaseFlag() == $flagId) {
          $this->messenger()->addMessage(t('Entity removed from the %flaglist list.',
            ['%flaglist' => $flagId]));
        }

      }
    }
  }

}
