<?php

namespace Drupal\flag_lists;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Flagging collection entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class FlagListItemHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $collection->get('entity.flag_list_item.edit_form')->setRequirement('_permission', 'administer flag lists');
    $collection->get('entity.flag_list_item.delete_form')->setRequirement('_permission', 'administer flag lists');
    return $collection;
  }

}
