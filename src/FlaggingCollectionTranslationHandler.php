<?php

namespace Drupal\flag_lists;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for flagging_collection.
 */
class FlaggingCollectionTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
