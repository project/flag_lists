<?php

namespace Drupal\flag_lists\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Flag for list entity.
 *
 * @ConfigEntityType(
 *   id = "flag_for_list",
 *   label = @Translation("Template Flag"),
 *   label_singular = @Translation("Template Flag"),
 *   label_plural = @Translation("Template Flags"),
 *   label_count = @PluralTranslation(
 *     singular = "@count template",
 *     plural = "@count templates"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\flag_lists\FlagForListListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\flag_lists\Form\FlagForListForm",
 *       "delete" = "Drupal\flag_lists\Form\FlagForListDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\flag_lists\FlagForListHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "flag_for_list",
 *   admin_permission = "administer flag lists",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "langcode",
 *     "status",
 *     "dependencies",
 *     "base_flag",
 *     "owner",
 *     "weight"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/flag_lists/{flag_for_list}",
 *     "edit-form" = "/admin/structure/flag_lists/{flag_for_list}/edit",
 *     "delete-form" = "/admin/structure/flag_lists/{flag_for_list}/delete",
 *     "collection" = "/admin/structure/flag_lists"
 *   }
 * )
 */
class FlagForList extends ConfigEntityBase implements FlagForListInterface {

  /**
   * The Flag for list ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Flag for list label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Base flag, i.e. the Flag template for this list.
   *
   * @var string
   */
  protected $base_flag = NULL;

  /**
   * The Owner of this list.
   *
   * @var string
   */
  protected $owner = NULL;

  /**
   * {@inheritdoc}
   */
  public function getBaseFlag() {
    return $this->base_flag;
  }

  /**
   * {@inheritdoc}
   */
  public function setBaseFlag($flag) {
    $this->base_flag = $flag;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->owner;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner($owner = NULL) {
    if (!empty($owner)) {
      $this->owner = $owner;
    }
    else {
      $account = \Drupal::currentUser();
      $this->owner = $account->getAccountName();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    // If a Flag For List is removed also remove the Flag this is based on.
    $flagService = \Drupal::service('flag');
    foreach ($entities as $entity) {
      $flag = $flagService->getFlagById($entity->getBaseFlag());
      // Make sure the flag entity exists.
      if (!empty($flag)) {
        $flag->delete();
      }
    }
    parent::preDelete($storage, $entities);
  }

  /**
   * Check if the base_flag (template) flag exist.
   *
   * @return bool
   *   Return the status of the base Flag.
   */
  public function hasBaseFlag() {
    $flag_id = $this->getBaseFlag();
    $flag = \Drupal::entityTypeManager()->getStorage('flag')->load($flag_id);

    return !empty($flag);
  }

}
