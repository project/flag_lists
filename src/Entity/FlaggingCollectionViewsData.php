<?php

namespace Drupal\flag_lists\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Flagging collection entities.
 */
class FlaggingCollectionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Add all Entities that have active Flagging Collections.
    foreach (\Drupal::service('flaglists')->getUsersFlaggingCollections() as
      $entity => $entityInfo) {
      $entityType = $entityInfo->getBaseFlag()->get('entity_type');
      if (empty($data[$entityType]['flagging_collection_bulk_form'])) {
        $data[$entityType]['flagging_collection_bulk_form']['field'] = [
          'title' => $this->t('Flagging Collection operations'),
          'help' => $this->t('Add a form element that lets you run operations on multiple entities.'),
          'id' => 'flagging_collection_bulk_form',
        ];
      }
    }
    // Remove "dangerous" data.
    unset($data['flagging_collection']['delete_flagging_collection']);

    return $data;
  }

}
