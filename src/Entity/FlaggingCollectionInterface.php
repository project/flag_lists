<?php

namespace Drupal\flag_lists\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Flagging collection entities.
 *
 * @ingroup flag_lists
 */
interface FlaggingCollectionInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Flagging collection name.
   *
   * @return string
   *   Name of the Flagging collection.
   */
  public function getName();

  /**
   * Sets the Flagging collection name.
   *
   * @param string $name
   *   The Flagging collection name.
   *
   * @return \Drupal\flag_lists\Entity\FlaggingCollectionInterface
   *   The called Flagging collection entity.
   */
  public function setName($name);

  /**
   * Gets the Flagging collection creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Flagging collection.
   */
  public function getCreatedTime();

  /**
   * Sets the Flagging collection creation timestamp.
   *
   * @param int $timestamp
   *   The Flagging collection creation timestamp.
   *
   * @return \Drupal\flag_lists\Entity\FlaggingCollectionInterface
   *   The called Flagging collection entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Flagging collection published status indicator.
   *
   * Unpublished Flagging collection are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Flagging collection is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Flagging collection.
   *
   * @param bool $published
   *   TRUE to set this Flagging collection to published,
   *   FALSE to set it to unpublished.
   *
   * @return \Drupal\flag_lists\Entity\FlaggingCollectionInterface
   *   The called Flagging collection entity.
   */
  public function setPublished($published);

  /**
   * Gets the Flagging collection revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Flagging collection revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\flag_lists\Entity\FlaggingCollectionInterface
   *   The called Flagging collection entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Flagging collection revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Flagging collection revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\flag_lists\Entity\FlaggingCollectionInterface
   *   The called Flagging collection entity.
   */
  public function setRevisionUserId($uid);

  /**
   * Gets the Base Flag for the Flagging collection.
   *
   * @return string
   *   Name of the Base Flag.
   */
  public function getBaseFlag();

  /**
   * Sets the Base Flag for this Flagging Collection.
   *
   * @param string $name
   *   The Base Flag name.
   *
   * @return \Drupal\flag_lists\Entity\FlaggingCollectionInterface
   *   The called Flagging collection entity.
   */
  public function setBaseFlag($name);

  /**
   * Gets the Related Flag for the Flagging collection.
   *
   * @return string
   *   Name of the Related Flag.
   */
  public function getRelatedFlag();

  /**
   * Sets the Related Flag for this Flagging Collection.
   *
   * @param string $relatedFlag
   *   The Related Flag name.
   *
   * @return \Drupal\flag_lists\Entity\FlaggingCollectionInterface
   *   The called Flagging collection entity.
   */
  public function setRelatedFlag($relatedFlag);

}
