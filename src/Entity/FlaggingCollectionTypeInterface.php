<?php

namespace Drupal\flag_lists\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Flagging collection type entities.
 */
interface FlaggingCollectionTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
