<?php

namespace Drupal\flag_lists\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Flag list item entities.
 */
class FlagListItemViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins,
    // can be put here.
    //
    // Connect the Flag List used.
    $data['flag_list_item_field_data']['flag_list'] = [
      'title' => $this->t('Flagging collection'),
      'help' => $this->t('The related Flagging collection'),
      'field' => [
        'id' => 'standard',
      ],
      'relationship' => [
        'base' => 'flagging_collection_field_data',
        'base field' => 'id',
        'id' => 'standard',
        'label' => $this->t('Connected flag'),
      ],
    ];

    // Connect the Entity Flagged.
    $data['flag_list_item_field_data']['entity_id'] = [
      'title' => $this->t('Node'),
      'help' => $this->t('The related Node'),
      'field' => [
        'id' => 'standard',
      ],
      'relationship' => [
        'base' => 'node_field_data',
        'base field' => 'nid',
        'id' => 'standard',
        'label' => $this->t('Connected node'),
      ],
    ];
    // Remove "dangerous" data.
    unset($data['flag_list_item']['delete_flag_list_item']);
    unset($data['flag_list_item']['edit_flag_list_item']);

    return $data;
  }

}
