<?php

namespace Drupal\flag_lists\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Flag list item entity.
 *
 * @ingroup flag_lists
 *
 * @ContentEntityType(
 *   id = "flag_list_item",
 *   label = @Translation("Flag list item"),
 *   label_singular = @Translation("Flag list item"),
 *   label_plural = @Translation("Flag list items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count flag list item",
 *     plural = "@count flag list items"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\flag_lists\FlagListItemListBuilder",
 *     "views_data" = "Drupal\flag_lists\Entity\FlagListItemViewsData",
 *     "translation" = "Drupal\flag_lists\FlagListItemTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\flag_lists\Form\FlagListItemForm",
 *       "delete" = "Drupal\flag_lists\Form\FlagListItemDeleteForm",
 *     },
 *     "access" = "Drupal\flag_lists\Access\FlagListItemAccessControlHandler",
 *
 *     "route_provider" = {
 *       "html" = "Drupal\flag_lists\FlagListItemHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "flag_list_item",
 *   data_table = "flag_list_item_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer flag lists",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/flag_lists/flag_list_item/flag_list_item/{flag_list_item}",
 *     "collection" = "/admin/structure/flag_lists/flag_list_item/flag_list_item",
 *     "edit-form" = "/admin/structure/flag_lists/flag_list_item/{flag_list_item}/edit",
 *     "delete-form" = "/admin/structure/flag_lists/flag_list_item/{flag_list_item}/delete",
 *   },
 * )
 */
class FlagListItem extends ContentEntityBase implements FlagListItemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFlag() {
    return $this->get('baseflag')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBaseFlag($baseFlag) {
    $this->set('baseflag', $baseFlag);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFlagList() {
    return $this->get('flag_list')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFlagList($flagList) {
    $this->set('flag_list', $flagList);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectedEntityId() {
    return $this->get('entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectedEntityId($entityId) {
    $this->set('entity_id', $entityId);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectedEntityType() {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectedEntityType($entityType) {
    $this->set('type', $entityType);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Flag list item entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Flag list item entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Flag list item is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['baseflag'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Base Flag'))
      ->setDescription(t('The base flag, template, used.'));

    $fields['flag_list'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Flag list'))
      ->setDescription(t('The id of Flag list this entity is connected to.'));

    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The Entity ID of the entity.'));

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('The Entity type.'));

    return $fields;
  }

}
