<?php

namespace Drupal\flag_lists\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Flagging collection type entity.
 *
 * @ConfigEntityType(
 *   id = "flagging_collection_type",
 *   label = @Translation("Flagging collection type"),
 *   label_singular = @Translation("Flagging collection type"),
 *   label_plural = @Translation("Flagging collection types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count flagging collection type",
 *     plural = "@count flagging collection types"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\flag_lists\FlaggingCollectionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\flag_lists\Form\FlaggingCollectionTypeForm",
 *       "edit" = "Drupal\flag_lists\Form\FlaggingCollectionTypeForm",
 *       "delete" = "Drupal\flag_lists\Form\FlaggingCollectionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\flag_lists\FlaggingCollectionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "flagging_collection_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "flagging_collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "langcode",
 *     "status",
 *     "dependencies"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/flag_lists/flagging_collection_type/{flagging_collection_type}",
 *     "add-form" = "/admin/structure/flag_lists/flagging_collection_type/add",
 *     "edit-form" = "/admin/structure/flag_lists/flagging_collection_type/{flagging_collection_type}/edit",
 *     "delete-form" = "/admin/structure/flag_lists/flagging_collection_type/{flagging_collection_type}/delete",
 *     "collection" = "/admin/structure/flag_lists/flagging_collection_type"
 *   }
 * )
 */
class FlaggingCollectionType extends ConfigEntityBundleBase implements FlaggingCollectionTypeInterface {

  /**
   * The Flagging collection type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Flagging collection type label.
   *
   * @var string
   */
  protected $label;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    return $uri_route_parameters;
  }

}
