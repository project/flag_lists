<?php

namespace Drupal\flag_lists\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Flag list item entities.
 *
 * @ingroup flag_lists
 */
interface FlagListItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Flag list item name.
   *
   * @return string
   *   Name of the Flag list item.
   */
  public function getName();

  /**
   * Sets the Flag list item name.
   *
   * @param string $name
   *   The Flag list item name.
   *
   * @return \Drupal\flag_lists\Entity\FlagListItemInterface
   *   The called Flag list item entity.
   */
  public function setName($name);

  /**
   * Gets the Flag list item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Flag list item.
   */
  public function getCreatedTime();

  /**
   * Sets the Flag list item creation timestamp.
   *
   * @param int $timestamp
   *   The Flag list item creation timestamp.
   *
   * @return \Drupal\flag_lists\Entity\FlagListItemInterface
   *   The called Flag list item entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Flag list item published status indicator.
   *
   * Unpublished Flag list item are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Flag list item is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Flag list item.
   *
   * @param bool $published
   *   TRUE to set this Flag list item to published,
   *   FALSE to set it to unpublished.
   *
   * @return \Drupal\flag_lists\Entity\FlagListItemInterface
   *   The called Flag list item entity.
   */
  public function setPublished($published);

  /**
   * Gets the Base Flag for this item.
   *
   * @return string
   *   Name of the Base Flag.
   */
  public function getBaseFlag();

  /**
   * Sets the Base Flag for this item.
   *
   * @param string $baseFlag
   *   The Base Flag name.
   *
   * @return \Drupal\flag_lists\Entity\FlagListItemInterface
   *   The called Flag list item entity.
   */
  public function setBaseFlag($baseFlag);

  /**
   * Gets the Flag List for this item.
   *
   * @return string
   *   Name of the Flag List.
   */
  public function getFlagList();

  /**
   * Sets the Flag List for this item.
   *
   * @param string $flagList
   *   The Flag List name.
   *
   * @return \Drupal\flag_lists\Entity\FlagListItemInterface
   *   The called Flag list item entity.
   */
  public function setFlagList($flagList);

  /**
   * Gets the entity Id this Flag List Item is connected to.
   *
   * @return int
   *   Id of the Entity connected.
   */
  public function getConnectedEntityId();

  /**
   * Sets the entity Id this Flag List Item is connected to.
   *
   * @param int $entityId
   *   The Entity Id.
   *
   * @return \Drupal\flag_lists\Entity\FlagListItemInterface
   *   The called Flag list item entity.
   */
  public function setConnectedEntityId($entityId);

  /**
   * Gets the type of the Entity connected.
   *
   * @return string
   *   Type of the Entity connected.
   */
  public function getConnectedEntityType();

  /**
   * Sets the type of the connected Entity.
   *
   * @param string $entityType
   *   The itype of the entity connected.
   *
   * @return \Drupal\flag_lists\Entity\FlagListItemInterface
   *   The called Flag list item entity.
   */
  public function setConnectedEntityType($entityType);

}
