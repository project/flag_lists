<?php

namespace Drupal\flag_lists\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Flag for list entities.
 */
interface FlagForListInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
