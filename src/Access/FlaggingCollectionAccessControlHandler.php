<?php

namespace Drupal\flag_lists\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Flagging collection entity.
 *
 * @see \Drupal\flag_lists\Entity\FlaggingCollection.
 */
class FlaggingCollectionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\flag_lists\Entity\FlaggingCollectionInterface $entity */
    switch ($operation) {
      case 'view':
        if ($account->id() == $entity->getOwner()->id()) {
          // These are my own flagging collections.
          return AccessResult::allowedIfHasPermission($account, 'view own flag lists');
        }
        elseif ($entity->getBaseFlag()->isGlobal()) {
          // These are global flagging collections.
          return AccessResult::allowedIfHasPermission($account, 'access global flag lists');
        }
        else {
          // If you have access to other flagging collections in a way.
          return AccessResult::allowedIfHasPermissions($account, [
            'administer flag lists',
            'view flag lists',
          ],
            'OR'
          );
        }
      case 'update':
      case 'delete':
        if ($account->id() == $entity->getOwner()->id()) {
          // These are my own flagging collections.
          return AccessResult::allowedIfHasPermission($account, 'edit own flag lists');
        }
        else {
          return AccessResult::allowedIfHasPermission($account, 'administer flag lists');
        }
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add flag lists');
  }

}
