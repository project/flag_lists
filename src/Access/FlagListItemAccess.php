<?php

namespace Drupal\flag_lists\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Flag list item entity.
 *
 * @see \Drupal\flag_lists\Entity\FlagListItem.
 */
class FlagListItemAccess {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function customAccess(AccountInterface $account) {
    // Only show the meny if the Migration module is installed.
    return AccessResult::allowedIfHasPermission($account,
      'administer flag lists')
      ->andIf(AccessResult::allowedIf(\Drupal::moduleHandler()
        ->moduleExists('migrate')));
  }

}
