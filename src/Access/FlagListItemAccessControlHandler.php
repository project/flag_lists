<?php

namespace Drupal\flag_lists\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Flag list item entity.
 *
 * @see \Drupal\flag_lists\Entity\FlagListItem.
 */
class FlagListItemAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\flag_lists\Entity\FlagListItemInterface $entity */
    switch ($operation) {
      case 'view':
        if ($account->id() == $entity->getOwner()->id()) {
          // These are my own Items.
          return AccessResult::allowedIfHasPermission($account, 'view own flag lists');
        }
        $flagService = \Drupal::service('flag');
        $baseFlag = $flagService->getFlagById($entity->getBaseFlag());
        if ($baseFlag->isGlobal()) {
          // This is Items from a global Flagging Collection.
          return AccessResult::allowedIfHasPermission($account, 'access global flag lists');
        }
        else {
          // Somehow we have access to the Items.
          return AccessResult::allowedIfHasPermissions($account, [
            'administer flag lists',
            'view flag lists',
          ],
            'OR'
          );
        }

      case 'update':
      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'edit own flag lists');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add flag lists');
  }

}
