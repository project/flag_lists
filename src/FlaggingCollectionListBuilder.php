<?php

namespace Drupal\flag_lists;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Flagging collection entities.
 *
 * @ingroup flag_lists
 */
class FlaggingCollectionListBuilder extends EntityListBuilder {

  /**
   * The logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */

  protected $logger;

  /**
   * Constructs a new FlaggingCollectionListBuilder object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger interface.
   */

  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, LoggerChannelInterface $logger) {
    parent::__construct($entity_type, $storage);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static($entity_type,
      $container->get('entity_type.manager')
        ->getStorage($entity_type
        ->id()),
      $container->get('flag_lists.logger.channel.flag_lists'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {

    $header['name'] = $this->t('Flagging Collection');
    $header['id'] = $this->t('ID');
    $header['bundle']['data'] = $this->t('Bundle');
    $header['templateflag']['data'] = $this->t('Flag for List');
    $header['relatedflag']['data'] = $this->t('The related flag');
    $header['base_flag']['data'] = $this->t('Template Flag');
    $header['owner']['data'] = $this->t('Owner');
    $header['scope']['data'] = $this->t('Scope');
    $header['operations']['data'] = $this->t('Operations');
    // Don't call parent as we add the Operations header ourselves
    // $header[] = parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**  @var \Drupal\flag_lists\Entity\FlaggingCollection $entity */

    $baseflag = $entity->getBaseFlag();
    $relatedflag = $entity->getRelatedFlag();
    $flagListService = \Drupal::service('flaglists');
    $flag = $flagListService->getFlagForListById($baseflag->id());
    $account = \Drupal::currentUser()->getAccount();

    if ($entity->access('view', $account)) {

      $row = [];

      $row['name'] = Link::createFromRoute(
        $entity->label(),
        'entity.flagging_collection.edit_form',
        ['flagging_collection' => $entity->id()]
      );
      $row['id'] = $entity->id();
      $bundle = Link::createFromRoute(
        $flagListService->getFlaggingCollectionTypeById(
          $entity->bundle())->label(),
          'entity.flagging_collection_type.collection');
      if ($bundle->getUrl()->access($account)) {
        $row['bundle'] = $bundle;
      }
      else {
        $row['bundle'] = $flagListService->getFlaggingCollectionTypeById(
          $entity->bundle())->label();
      }
      $templateFlag = Link::createFromRoute(
        $flagListService->getFlagForListById($baseflag->id())->label(),
        'entity.flag_for_list.collection'
      );
      if ($templateFlag->getUrl()->access($account)) {
        $row['templateflag'] = $templateFlag;
      }
      else {
        $row['templateFlag'] = $flagListService->getFlagForListById(
          $baseflag->id())->label();
      }

      if (!empty($relatedflag)) {
        $row['relatedflag'] = $relatedflag->label();
      }
      else {
        $row['relatedflag'] = 'Missing/Deleted!';
        $row['relatedflag'] = Link::createFromRoute(
          $this->t('Missing/Deleted'),
            'flag_lists.missing_related');
        $this->logger
          ->critical('Your Related Flag for your Flag List "@flag_list" is missing. Please check your installation!',
          ['@flag_list' => $entity->label() ]);
      }
      $row['base_flag'] = $baseflag->label();
      $row['owner'] = $entity->getOwner()->label();
      $row['scope'] = $baseflag->isGlobal() ? $this->t('Global') : $this->t('Personal');

      return $row + parent::buildRow($entity);
    }
  }

  /**
   * {@inheritdoc}
   *
   * Do sorting based on number of operations.
   */
  public function render() {
    $build = parent::render();
    if (!empty($build['table']['#rows'])) {
      uasort($build['table']['#rows'],
        [$this, 'numberOfOperations']
      );
    }
    return $build;
  }

  /**
   * Check how many operations links there are.
   *
   * This is a helper to sort Flagging Collections with less
   * operation items, normally indicating collection not owned
   * by "me".
   */
  private function numberOfOperations($a, $b) {

    if (count($a['operations']['data']['#links']) ==
      count($b['operations']['data']['#links'])) {
      return 0;
    }
    // Note the reverse sort order!
    return count($a['operations']['data']['#links']) <
      count($b['operations']['data']['#links']) ?
      1 : -1;
  }

}
