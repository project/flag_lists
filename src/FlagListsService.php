<?php

namespace Drupal\flag_lists;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class FlagForList service.
 *
 *  Handles search requests for flags and flaggings.
 *
 *  Performs flagging and unflaging operations.
 *  Most of it SWP (Stolen With Pride) from flag module.
 */
class FlagListsService implements FlagListsServiceInterface {

  /**
   * The current user injected into the service.
   *
   * @var Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * The type Entity Type Manager injected into the service.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(AccountInterface $current_user,
                              EntityTypeManagerInterface $entity_type_manager) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get all Flag For List, i.e. all Flags tagged as available.
   *
   * @param string $entity_type
   *   The entity wanted entity type.
   * @param string $bundle
   *   The wanted bundle of the entity.
   *
   * @return Drupal\flag_lists\Entity\FlagForList[]
   *   An array of found Flag For Lists.
   */
  public function getAllFlagForList($entity_type = NULL, $bundle = NULL) {
    $query = $this->entityTypeManager
      ->getStorage('flag_for_list')
      ->getQuery();

    if ($entity_type != NULL) {
      $query->condition('entity_type', $entity_type);
    }

    $ids = $query->accessCheck()->execute();
    $flags = $this->getFlagForListByIds($ids);

    if (isset($bundle)) {
      $flags = array_filter($flags, function (FlagForList $flag) use ($bundle) {
        $bundles = $flag->getApplicableBundles();
        return in_array($bundle, $bundles);
      });
    }

    return $flags;
  }

  /**
   * Get current user's Flagging Collections.
   *
   * @return Drupal\flag_lists\Entity\FlaggingCollection[]
   *   An array of current User's Flagging Collections.
   */
  public function getUsersFlaggingCollections(
    AccountInterface $account = NULL) {
    $query = $this->entityTypeManager
      ->getStorage('flagging_collection')
      ->getQuery();
    if ($account <> NULL) {
      $query->condition('user_id', $account->id());
    }
    else {
      $query->condition('user_id', $this->currentUser->id());
    }

    $ids = $query->accessCheck()->execute();
    $flags = $this->getFlaggingCollectionByIds($ids);

    return $flags;
  }

  /**
   * Get all Flagging Collections.
   *
   * @param string $type
   *   The type, bundle, of the Flagging Collection.
   *
   * @return Drupal\flag_lists\Entity\FlaggingCollection[]
   *   An array of the found Flagging Collections.
   */
  public function getAllFlaggingCollections($type = NULL) {
    $query = $this->entityTypeManager
      ->getStorage('flagging_collection')
      ->getQuery();

    if ($type != NULL) {
      $query->condition('type', $type);
    }

    $ids = $query->accessCheck()->execute();
    $flags = $this->getFlaggingCollectionByIds($ids);

    return $flags;
  }

  /**
   * Get a user's Flag For List, i.e. all Flags tagged as available.
   *
   * NOT used nor tested!
   *
   * @param Drupal\Core\Session\AccountInterface $account
   *   The wanted user's account.
   * @param string $entity_type
   *   The wanted entity type.
   * @param string $bundle
   *   The wanted bundle of the entity.
   *
   * @return Drupal\flag_lists\Entity\FlagForList[]
   *   An array of Flag For Lists that the account has access to.
   */
  public function getUsersFlagForList(AccountInterface $account, $entity_type = NULL, $bundle = NULL) {
    $flagForList = $this->getAllFlagForList($entity_type, $bundle);

    $filteredFlagForList = [];
    foreach ($flagForList as $flag_id => $flag) {
      if ($flag->actionAccess('flag', $account)->isAllowed() ||
          $flag->actionAccess('unflag', $account)->isAllowed()) {
        $filteredFlagForList[$flag_id] = $flag;
      }
    }

    return $filteredFlagForList;
  }

  /**
   * Get a Flag For List, i.e. the Flags tagged as available as templates.
   *
   * @param int $flag_id
   *   The template id to load.
   *
   * @return Drupal\flag_lists\Entity\FlagForList[]
   *   An array of Flag For Lists.
   */
  public function getFlagForListById($flag_id) {
    return $this->entityTypeManager->getStorage('flag_for_list')->load($flag_id);
  }

  /**
   * Get a Flagging Collection by Id.
   *
   * @param string $flag_id
   *   The flag Id of the Flagging Collection to load.
   *
   * @return Drupal\flag_lists\Entity\FlaggingCollection
   *   The loaded Flagging Collection.
   */
  public function getFlaggingCollectionById($flag_id) {
    return $this->entityTypeManager->getStorage('flagging_collection')->load($flag_id);
  }

  /**
   * Loads Flag For Lists by ids.
   *
   * @param int[] $ids
   *   The Ids of the Flag For Lists to load.
   *
   * @return Drupal\flag_lists\Entity\FlagForList[]
   *   The loaded Flag For Lists.
   */
  protected function getFlagForListByIds(array $ids) {
    return $this->entityTypeManager->getStorage('flag_for_list')->loadMultiple($ids);
  }

  /**
   * Loads Flagging Collections by ids.
   *
   * @param int[] $ids
   *   The Ids of the Flag For Lists to load.
   *
   * @return Drupal\flag_lists\Entity\FlaggingCollection
   *   The loaded Flagging Collections.
   */
  protected function getFlaggingCollectionByIds(array $ids) {
    return $this->entityTypeManager->getStorage('flagging_collection')->loadMultiple($ids);
  }

  /**
   * Load a Flagging Collection Type by Id.
   *
   * @param string $type_id
   *   The Id of the Flagging Collection Type to load.
   *
   * @return Drupal\flag_lists\Entity\FlaggingCollectionType
   *   The loaded Flagging Collection Type
   */
  public function getFlaggingCollectionTypeById($type_id) {
    return $this->entityTypeManager->getStorage('flagging_collection_type')->load($type_id);
  }

  /**
   * Load all Flagging Collection Types.
   *
   * @return Drupal\flag_lists\Entity\FlaggingCollectionType
   *   The loaded Flagging Collection Type
   */
  public function getAllFlaggingCollectionTypes() {
    return $this->entityTypeManager->getStorage('flagging_collection_type')->loadMultiple();
  }

  /**
   * Get Flag For List ids.
   *
   * @param string $baseflag
   *   The base flag to use.
   * @param string $flag_list
   *   The flag list.
   * @param int $entity_id
   *   The entity id.
   *
   * @return int[]
   *   An array of ids for the Flag For Lists
   */
  public function getFlagListItemIds($baseflag,
      $flag_list = NULL,
      $entity_id = NULL) {
    $query = $this->entityTypeManager
      ->getStorage('flag_list_item')
      ->getQuery();
    $query->condition('baseflag', $baseflag);
    if ($flag_list <> NULL) {
      $query->condition('flag_list', $flag_list);
    }
    if ($entity_id <> NULL) {
      $query->condition('entity_id', $entity_id);
    }
    $ids = $query->accessCheck()->execute();
    return $ids;
  }

  /**
   * Get Flag List Items.
   *
   * @param int[] $ids
   *   The Flag List Item ids to load.
   *
   * @return Drupal\flag_lists\Entity\FlagListItem[]
   *   An array of Flag List Items.
   */
  public function getFlagListItems(array $ids) {
    return $this->entityTypeManager->getStorage('flag_list_item')->loadMultiple($ids);
  }

  /**
   * Get Flaggging Collection ids by name.
   *
   * @param string $name
   *   The name of the Flagging Collection to load.
   *
   * @return int[]
   *   The Flagging Collection id
   */
  public function getFlaggingCollectionIdByName($name) {
    $query = $this->entityTypeManager
      ->getStorage('flagging_collection')
      ->getQuery();
    $query->condition('name', $name);
    $id = $query->accessCheck()->execute();
    return $id;
  }

  /**
   * Get Flaggging Collection ids by related flag.
   *
   * @param string $name
   *   The name of the Related Flag to Flagging Collection to load.
   *
   * @return int[]
   *   The Flagging Collection id
   */
  public function getFlaggingCollectionIdByRelated($name) {
    $query = $this->entityTypeManager
      ->getStorage('flagging_collection')
      ->getQuery();
    $query->condition('relatedflag', $name);
    $id = $query->accessCheck()->execute();
    return $id;
  }

}
