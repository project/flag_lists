<?php

namespace Drupal\flag_lists;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\flag_lists\Entity\FlaggingCollectionInterface;

/**
 * Defines the storage handler class for Flagging collection entities.
 *
 * This extends the base storage class, adding required special handling for
 * Flagging collection entities.
 *
 * @ingroup flag_lists
 */
class FlaggingCollectionStorage extends SqlContentEntityStorage implements FlaggingCollectionStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(FlaggingCollectionInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {flagging_collection_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {flagging_collection_field_revision} WHERE user_id = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(FlaggingCollectionInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {flagging_collection_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('flagging_collection_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
