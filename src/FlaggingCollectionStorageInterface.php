<?php

namespace Drupal\flag_lists;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\flag_lists\Entity\FlaggingCollectionInterface;

/**
 * Defines the storage handler class for Flagging collection entities.
 *
 * This extends the base storage class, adding required special handling for
 * Flagging collection entities.
 *
 * @ingroup flag_lists
 */
interface FlaggingCollectionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Flagging collection revision IDs for a collection.
   *
   * @param \Drupal\flag_lists\Entity\FlaggingCollectionInterface $entity
   *   The Flagging collection entity.
   *
   * @return int[]
   *   Flagging collection revision IDs (in ascending order).
   */
  public function revisionIds(FlaggingCollectionInterface $entity);

  /**
   * Gets a list of revision IDs owned by a given user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Flagging collection revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\flag_lists\Entity\FlaggingCollectionInterface $entity
   *   The Flagging collection entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(FlaggingCollectionInterface $entity);

  /**
   * Unsets the language for all Flagging collection with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
