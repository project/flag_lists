<?php

namespace Drupal\flag_lists;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for flag_list_item.
 */
class FlagListItemTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
