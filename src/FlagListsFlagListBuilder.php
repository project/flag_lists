<?php

namespace Drupal\flag_lists;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\flag\Controller\FlagListBuilder;

/**
 * Defines a class to build an enhanced listing of Flag entities.
 *
 * @ingroup flag_lists
 */
class FlagListsFlagListBuilder extends FlagListBuilder {

  /**
   * {@inheritdoc}
   *
   * The type Config Factory injected into the service.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   *   The Injected Config Factory.
   */
  protected $configFactory;

  /**
   * The Flag Lists service injected into the List Builder.
   *
   * @var Drupal\flag_lists\FlagListsService
   *   The Injected Flag Lists Service.
   */
  protected $flagListsService;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('flaglists')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Constructor.
   *
   * @param Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type involved.
   * @param Drupal\Core\Entity\EntityTypeManagerInteface $entity_type_manager
   *   The entity type manageri service.
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param Drupal\flag_lists\FlagListsService $flag_lists_service
   *   The Flag Lists Service.
   */
  public function __construct(EntityTypeInterface $entity_type,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    FlagListsService $flag_lists_service) {
    parent::__construct($entity_type, $entity_type_manager);
    $this->configFactory = $config_factory;
    $this->flagListsService = $flag_lists_service;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $total = parent::buildHeader();
    $header['flagListUsage'] = $this->t('Flag Lists Usage');
    $header['creator'] = $this->t('Creator');
    $first_array = array_splice($total, 0, 5);
    $total = array_merge($first_array, $header, $total);
    return $total;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\flag_lists\Entity\FlaggingCollection $entity */

    $row['flagListUsage']['#markup'] = $this->t('-');
    $row['creator']['#markup'] = $this->t('-');
    $total = parent::buildRow($entity);
    if (!empty($this->flagListsService->getFlagForListById($entity->id()))) {
      $row['flagListUsage']['#markup'] = $this->t('Template');
    }
    if (!empty($this->flagListsService->getflaggingCollectionIdByRelated($entity->id()))) {

      $config = $this->configFactory->get('flag_lists.settings');
      if ($config->get('hide_collections') == 1) {
        // Don't list the Flagging Collections in the overview.
        return NULL;
      };
      $row['flagListUsage']['#markup'] = $this->t('Flagging Collection');
      $flaggingCollectionId = $this->flagListsService->getflaggingCollectionIdByRelated($entity->id());
      $flaggingCollectionId = array_key_first($flaggingCollectionId);
      $row['creator']['#markup'] = $this->flagListsService
        ->getFlaggingCollectionById($flaggingCollectionId)
        ->getOwner()
       ->getDisplayName();
      // Add the Flagging Collection Canonical link.
      $flaggingCollectionId =
        implode("|", $this
          ->flagListsService
          ->getflaggingCollectionIdByRelated($entity->id()));
      $firstItem['view']['title'] =
        $this->t('View');
      $firstItem['view']['weight'] = 10;
      $firstItem['view']['url'] =
        Url::fromRoute('entity.flagging_collection.canonical',
          ['flagging_collection' => $flaggingCollectionId]);
      $subTotal = $firstItem + $total['operations']['data']['#links'];
      $total['operations']['data']['#links'] = $subTotal;
      // Also unset some operations that must be handle at other places.
      unset($total['operations']['data']['#links']['edit']);
      unset($total['operations']['data']['#links']['disable']);
      unset($total['operations']['data']['#links']['delete']);
      unset($total['operations']['data']['#links']['reset']);
      unset($total['operations']['data']['#links']['manage-fields']);
      unset($total['operations']['data']['#links']['manage-form-display']);
      unset($total['operations']['data']['#links']['manage-display']);
    }
    $first_array = array_splice($total, 0, 5);
    $total = array_merge($first_array, $row, $total);
    return $total;
  }

}
