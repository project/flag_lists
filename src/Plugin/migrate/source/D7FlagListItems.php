<?php

namespace Drupal\flag_lists\Plugin\migrate\source;

use Drupal\user\Entity\User;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "d7_flag_list_items",
 *   source_module = "flag_lists"
 * )
 */
class D7FlagListItems extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'curling_games' table.
    $query = $this->select('flag_lists_content', 'c');
    $query->join('flag_lists_flags', 'f', 'c.fid = f.fid');
    $query->fields('c', [
      'fcid',
      'fid',
      'entity_type',
      'entity_id',
      'uid',
      'sid',
      'timestamp',
    ])
      ->fields('f', [
        'name',
        'title',
      ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'fcid' => $this->t('Flag content id'),
      'fid' => $this->t('Flag lists id #'),
      'entity_type' => $this->t('Entity type'),
      'entity_id' => $this->t('Entity #'),
      'uid' => $this->t('Owner'),
      'sid' => $this->t('Sid'),
      'timestamp' => $this->t('Timestamp'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'fcid' => [
        'type' => 'integer',
        'alias' => 'f',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    $messenger = \Drupal::messenger();
    $logger = \Drupal::logger('flag_lists');

    // Check and get the user name.
    $uid = $row->getSourceProperty('uid');
    $user = User::load($uid);
    if (!empty($user)) {
      $owner = $uid;
    }
    else {
      $owner = 1;
    }
    $row->setSourceProperty('uid', $owner);

    // Check if the flagging collection exist.
    $found = FALSE;
    $flagListsService = \Drupal::service('flaglists');
    $baseFlags = $flagListsService->getAllFlaggingCollections();
    foreach ($baseFlags as $flag) {
      if ($found =
        $flag->get('id')->value == $row->getSourceProperty('fid')) {
        $relatedFlag = $flag->get('relatedflag')->getValue();
        $relatedFlagList = $relatedFlag['0']['target_id'];
        $row->setSourceProperty('relatedflag', $relatedFlagList);
        break;
      }
    }
    if (!$found) {
      $message = $this->t('The flagging collection "@collection" wasn\'t found');
      $messenger->addError($message,
        ['@collection' => $row->getSourceProperty('relatedflag')]);
      $logger->error($message,
        ['@collection' => $row->getSourceProperty('relatedflag')]);
    }

    // Check if the entity exists.
    $entity_id = $row->getSourceProperty('entity_id');
    $entity = \Drupal::entityTypeManager()->getStorage($row->getSourceProperty('entity_type'))->load($entity_id);
    if (empty($entity)) {
      $message = $this->t('The entity with ID "@entity_id" wasn\'t found',
        ['@entity_id' => $entity_id]);
      $messenger->addError($message);
      $logger->error($message);
    }

    return parent::prepareRow($row);
  }

}
