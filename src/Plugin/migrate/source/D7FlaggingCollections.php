<?php

namespace Drupal\flag_lists\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;
use Drupal\user\Entity\User;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "d7_flagging_collections",
 *   source_module = "flag_lists",
 * )
 */
class D7FlaggingCollections extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'flag_lists_flags' table.
    $query = $this->select('flag_lists_flags', 'c');
    $query->join('flag', 'f', 'c.pfid = f.fid');
    $query->fields('c', [
      'fid',
      'pfid',
      'uid',
      'entity_type',
      'title',
      'options',
    ])
      ->fields('f', [
        'name',
      ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'fid' => $this->t('Flag List #'),
      'pfid' => $this->t('Parent flag id #'),
      'uid' => $this->t('Owner'),
      'entity_type' => $this->t('Entity type'),
      'name' => $this->t('Machine name of the related flag'),
      'title' => $this->t('Name of flag list'),
      'option' => $this->t('Serielized info'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'fid' => [
        'type' => 'integer',
        'alias' => 'c',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // Check if the user exists.
    $uid = $row->getSourceProperty('uid');
    $user = User::load($uid);
    if (!empty($user)) {
      $owner = $uid;
    }
    else {
      // Make the Administrator the owner.
      $owner = 1;
    }
    $row->setSourceProperty('uid', $owner);

    // Check if the template flag exist.
    $found = FALSE;
    $flagService = \Drupal::service('flag');
    $templateFlags = $flagService->getAllFlags(
      $row->getSourceProperty('entity_type'));
    foreach ($templateFlags as $flag) {
      if ($found =
        $flag->get('id') == $row->getSourceProperty('name')) {
        break;
      }
    }
    if (!$found) {
      $message = $this->t('The template flag "@flag" wasn\'t found. Using fallback.',
        ['@flag' => $row->getSourceProperty('name')]);
      $messenger = \Drupal::messenger();
      $logger = \Drupal::logger('flag_lists');
      $messenger->addWarning($message);
      $logger->warning($message);

      // Fall back to known existing flag.
      $row->setSourceProperty('name', 'flag_list_template_1');
    }

    return parent::prepareRow($row);
  }

}
