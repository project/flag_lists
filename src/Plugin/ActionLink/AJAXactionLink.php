<?php

namespace Drupal\flag_lists\Plugin\ActionLink;

use Drupal\Core\Entity\EntityInterface;
use Drupal\flag\FlagInterface;
use Drupal\flag\Plugin\ActionLink\AJAXactionLink as FlagAJAXactionLink;

/**
 * Provides the AJAX link type.
 *
 * This class is an extension of the Reload link type, but modified to
 * provide AJAX links.
 *
 * @ActionLinkType(
 *   id = "ajax_link",
 *   label = @Translation("AJAX link"),
 *   description = "An AJAX JavaScript request will be made without
 *   reloading the page."
 * )
 */
class AJAXactionLink extends FlagAJAXactionLink {

  /**
   * {@inheritdoc}
   *
   * Currently a stub only.
   */
  public function getAsFlagLink(FlagInterface $flag, EntityInterface $entity,
       string $view_mode = NULL): array {
    $build = parent::getAsFlagLink($flag, $entity, $view_mode);
    // $build['#flagging_collection'] = 'text'.
    return $build;

  }

}
