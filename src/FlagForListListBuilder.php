<?php

namespace Drupal\flag_lists;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a listing of Flag for list entities.
 */
class FlagForListListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Template name');
    $header['id'] = $this->t('Machine name');
    $header['owner'] = ['data' => $this->t('Owner')];
    $header['base_flag'] = ['data' => $this->t('Template flag')];
    $header['entity_type'] = ['data' => $this->t('Entity')];
    $header['bundles'] = ['data' => $this->t('Bundles')];
    $header['scope'] = ['data' => $this->t('Scope')];
    // $header['weight'] = ['data' => $this->t('Weight')];
    $header['operations'] = ['data' => $this->t('Operations')];
    $header[] = parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row = [];
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['owner'] = $entity->get('owner');

    if ($entity->hasBaseFlag()) {
      $flag = \Drupal::service('flag')->getFlagById($entity->getBaseFlag());
      $flag_url = new Url('');
      $row['base_flag'] =
        Link::fromTextAndUrl(
          $flag->label(),
          $flag_url->fromRoute('entity.flag.edit_form',
            ['flag' => $flag->id()]))
          ->toString();

      $bundles = '';
      foreach ($flag->getBundles() as $bundle) {
        if (empty($bundles)) {
          $bundles = $bundle;
        }
        else {
          $bundles = $bundles . ', ' . $bundle;
        }
      }

      if (empty($bundles)) {
        $bundles = '-';
      }
      $row['entity_type'] = $flag->getFlaggableEntityTypeId();
      $row['bundles'] = $bundles;
      $row['scope'] = $flag->isGlobal() ? $this->t('Global') : $this->t('Personal');
    }
    else {
      $row['base_flag'] = $this->t("<em>Template flag doesn't exist</em>");
      $row['entity_type'] = 'unknown';
      $row['bundles'] = '-';
      $row['scope'] = '-';
    }
    return $row + parent::buildRow($entity);
  }

}
