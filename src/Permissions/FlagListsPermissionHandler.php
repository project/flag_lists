<?php

namespace Drupal\flag_lists\Permissions;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\CallableResolver;
use Drupal\user\PermissionHandler;
use Drupal\flag_lists\FlagListsServiceInterface;

/**
 * {@inheritdoc}
 */
class FlagListsPermissionHandler extends PermissionHandler {

  use StringTranslationTrait;

  /**
   * The Flag Lists Service.
   *
   * @var \Drupal\flag_lists\FlagListsServiceInterface
   */
  protected $flagListsService;

  /**
   * Constructs a new PermissionHandler.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   *  @param \Drupal\Core\Utility\CallableResolver $callable_resolver
   *   The callable resolver.
   * @param \Drupal\flag_lists\FlagListsServiceInterface $flag_lists_service
   *   The Flag Lists Service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list.
   */
  public function __construct(ModuleHandlerInterface $module_handler, TranslationInterface $string_translation, CallableResolver $callable_resolver, ModuleExtensionList $module_extension_list, FlagListsServiceInterface $flag_lists_service) {
    parent::__construct($module_handler, $string_translation, $callable_resolver, $module_extension_list);
    $this->flagListsService = $flag_lists_service;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    $all_permissions = $this->buildPermissionsYaml();
    // Remove the possible long list of unused flag permissions
    // due to the flag lists module.
    $flagLists = $this->flagListsService->getAllFlaggingCollections();
    foreach ($flagLists as $flagList) {
      if (!empty($flagList->getRelatedFlag())) {
        $flag = 'flag ' . $flagList->getRelatedFlag()->id();
        $unflag = 'unflag ' . $flagList->getRelatedFlag()->id();
        unset($all_permissions[$flag]);
        unset($all_permissions[$unflag]);
      }
    }
    // Check for access for the used template as well.
    $flagTemplates = $this->flagListsService->getAllFlagForList();
    foreach ($flagTemplates as $flagTemplate) {
      // Do we really want to remove the templates from
      // the possibility of setting them separately?
      $flag = 'flag ' . $flagTemplate->id();
      $unflag = 'unflag ' . $flagTemplate->id();
      unset($all_permissions[$flag]);
      unset($all_permissions[$unflag]);
    }
    return $this->sortPermissions($all_permissions);
  }

}
