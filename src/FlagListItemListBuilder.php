<?php

namespace Drupal\flag_lists;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Flag list item entities.
 *
 * @ingroup flag_lists
 */
class FlagListItemListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Flag list item ID');
    $header['name'] = $this->t('Name');
    $header['entity_id'] = $this->t('Entity ID');
    $header['entity_exist'] = $this->t('Entity exist?');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\flag_lists\Entity\FlagListItem $entity */
    $account = \Drupal::currentUser()->getAccount();
    $entity_id = $entity->getConnectedEntityId();
    $connectedEntity = \Drupal::entityTypeManager()
      ->getStorage('node')->load($entity_id);
    if ($entity->access('view', $account)) {
      $row['id'] = $entity->id();
      $row['name'] = Link::createFromRoute(
        $entity->label(),
        'entity.flag_list_item.edit_form',
        ['flag_list_item' => $entity->id()]
      );
      $row['entity_id'] = $entity_id;
      $row['entity_exist']['data'] =
        empty($connectedEntity) ? "Doesn't exist" : 'Exist';
      $row['entity_exist']['class'] =
        empty($connectedEntity) ? "entity-missing" : 'entity-exist';
      return $row + parent::buildRow($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#attached']['library'][] = 'flag_lists/flag_lists.table';
    return $build;
  }

}
