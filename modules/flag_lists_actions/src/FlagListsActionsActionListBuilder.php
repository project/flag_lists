<?php

namespace Drupal\flag_lists_actions;

use Drupal\Core\Action\ActionManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\action\ActionListBuilder;
use Drupal\flag_lists\FlagListsServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of action entities.
 *
 * @see \Drupal\system\Entity\Action
 * @see action_entity_type_build()
 */
class FlagListsActionsActionListBuilder extends ActionListBuilder {

  /**
   * The Flag Lists Service.
   *
   * @var \Drupal\flag_lists\FlagListsService
   */
  protected $flagListsService;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ActionListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The action storage.
   * @param \Drupal\Core\Action\ActionManager $action_manager
   *   The action plugin manager.
   * @param \Drupal\flag_lists\FlagListsServiceInterface $flag_lists_service
   *   The Flag Lists service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ActionManager $action_manager, FlagListsServiceInterface $flag_lists_service, ConfigFactoryInterface $config_factory) {
    parent::__construct($entity_type, $storage, $action_manager);

    $this->flagListsService = $flag_lists_service;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.action'),
      $container->get('flaglists'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if ($this->hasConfigurableActions) {
      if (!empty($entity->get('configuration')['flag_id'])) {
        if ($this->flagListsService
          ->getFlaggingCollectionIdByRelated($entity
            ->get('configuration')['flag_id'])) {
          $config = $this->configFactory->get('flag_lists.settings');
          if ($config->get('hide_actions') == 1) {
            return NULL;
          }
        }
      }
      $row['type'] = $entity->getType();
      $row['label'] = $entity->label();
      $row += parent::buildRow($entity);
      return $row;
    }
  }

}
