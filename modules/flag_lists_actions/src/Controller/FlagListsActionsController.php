<?php

namespace Drupal\flag_lists_actions\Controller;

use Drupal\system\Entity\Action;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class FlagListsActionsController.
 *
 *  Handle adding and deletion of Flagging Collections actions.
 */
class FlagListsActionsController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Add Flag Lists Actions.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create the Actions for.
   */
  public static function createActions(EntityInterface $entity) {
    $flag = $entity->getRelatedFlag();

    // Add the flag/unflag actions for this flag and entity combination.
    $flag_id = 'flag_action.' . $flag->id() . '_flag';
    if (!Action::load($flag_id)) {
      $action = Action::create([
        'id' => $flag_id,
        'type' => $flag->getFlaggableEntityTypeId(),
        'label' => \Drupal::token()->replace($flag->getShortText('flag'),
          ['flagging_collection' => $entity],
          ['clear']),
        'plugin' => 'flag_action:' . $flag->id() . '_flag',
        'configuration' => [
          'flag_id' => $flag->id(),
          'flag_action' => 'flag',
        ],
      ]);
      $action->trustData()->save();
    }
    $unflag_id = 'flag_action.' . $flag->id() . '_unflag';
    if (!Action::load($unflag_id)) {
      $action = Action::create([
        'id' => $unflag_id,
        'type' => $flag->getFlaggableEntityTypeId(),
        'label' => \Drupal::token()->replace($flag->getShortText('unflag'),
          ['flagging_collection' => $entity],
          ['clear']),
        'plugin' => 'flag_action:' . $flag->id() . '_unflag',
        'configuration' => [
          'flag_id' => $flag->id(),
          'flag_action' => 'unflag',
        ],
      ]);
      $action->trustData()->save();
    }
  }

  /**
   * Delete Flag Lists Actions.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The entity to delete the Actions for.
   */
  public static function deleteActions(EntityInterface $entity) {
    $flag = $entity->getRelatedFlag();

    $actions = Action::loadMultiple([
      'flag_action.' . $flag->id() . '_flag',
      'flag_action.' . $flag->id() . '_unflag',
    ]);

    // Remove the flag/unflag actions for this flag and entity combination.
    foreach ($actions as $action) {
      $action->delete();
    }
  }

}
