<?php

/**
 * @file
 * Contains flagging_collection.page.inc.
 *
 * Page callback for Flagging collection entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Flagging collection templates.
 *
 * Default template: flagging_collection.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_flagging_collection(array &$variables) {
  // Fetch FlaggingCollection Entity Object.
  $flagging_collection = $variables['elements']['#flagging_collection'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
