<?php

namespace Drupal\Tests\flag_lists\Unit;

use Drupal\flag_lists\Entity\FlagForList;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the FlagForLists class.
 *
 * @group flag_lists
 */
class FlagForListTest extends UnitTestCase {

  /**
   * Test the creating of FlagForLists.
   */
  public function testCreate() {
    $flagForList = new flagForList([], '');
    $this->assertInstanceOf(FlagForList::class, $flagForList);
  }

}
